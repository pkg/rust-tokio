rust-tokio (1.24.2-1+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 16 Jan 2025 17:26:09 +0100

rust-tokio (1.24.2-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 19:23:06 +0530

rust-tokio (1.24.2-1) unstable; urgency=medium

  * Team upload.
  * Package tokio 1.24.2 from crates.io using debcargo 2.6.0
    + New upstream fixes CVE-2023-22466 which I don't think affects
      Debian. (Closes: #1029157 )
  * Update revert-switch-from-winapi-to-windows-sys.patch for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 18 Jan 2023 22:31:43 +0000

rust-tokio (1.23.0-1) unstable; urgency=medium

  * Team upload.
  * Package tokio 1.23.0 from crates.io using debcargo 2.6.0 (Closes: #1026805)
  * Revert upstream switch from winapi to windows-sys.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 22 Dec 2022 22:15:49 +0000

rust-tokio (1.21.0-1) unstable; urgency=medium

  * Team upload.
  * Package tokio 1.21.0 from crates.io using debcargo 2.5.0
  * Drop relax-dep.patch no longer needed.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 04 Sep 2022 20:24:57 +0000

rust-tokio (1.19.2-1) unstable; urgency=medium

  * Team upload.
  * Package tokio 1.19.2 from crates.io using debcargo 2.5.0

 -- Peter Michael Green <plugwash@debian.org>  Sun, 19 Jun 2022 16:16:30 +0000

rust-tokio (1.18.2-3) unstable; urgency=medium

  * Team upload.
  * Package tokio 1.18.2 from crates.io using debcargo 2.5.0
  * Re-enable tracing feature (Closes: 1012337)

 -- Peter Michael Green <plugwash@debian.org>  Sun, 05 Jun 2022 12:59:09 +0000

rust-tokio (1.18.2-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Package tokio 1.18.2 from crates.io using debcargo 2.5.0

 -- James McCoy <jamessan@debian.org>  Sat, 04 Jun 2022 16:01:02 -0400

rust-tokio (1.18.2-1) experimental; urgency=medium

  * Team upload.
  * Package tokio 1.18.2 from crates.io using debcargo 2.5.0

 -- James McCoy <jamessan@debian.org>  Wed, 01 Jun 2022 21:47:17 -0400

rust-tokio (1.15.0-1) unstable; urgency=medium

  * Team upload.
  * Package tokio 1.15.0 from crates.io using debcargo 2.5.0
  * Stop altering dependency on parking-lot, we now have 0.11 in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 31 Dec 2021 11:21:05 +0000

rust-tokio (1.13.0-1) unstable; urgency=medium

  * Team upload.
  * d/patches: added relax-deps.patch
  * d/debcargo.toml: enabled collapse_features
  * Package tokio 1.13.0 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 28 Nov 2021 23:19:47 +0100

rust-tokio (0.1.14-2) unstable; urgency=medium

  * Source-only reupload for bullseye

 -- kpcyrd <git@rxv.cc>  Wed, 17 Jul 2019 05:19:32 +0000

rust-tokio (0.1.14-1) unstable; urgency=medium

  * Package tokio 0.1.14 from crates.io using debcargo 2.2.9

 -- kpcyrd <git@rxv.cc>  Thu, 24 Jan 2019 10:28:50 +0100
